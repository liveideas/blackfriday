// Remoty Alert Dialogs Plugin
//
// Version 1.0
//
// Gustavo G. Faccenda
function rConfirmV2(title, message, function_ok) {
		if ($("#modalConfirm").length > 0){
			  // do something here
			$("#modalConfirm").remove();
		}
		
		//alert($("#modalConfirm").length);
		$("BODY").append(
        '<div id="modalConfirm" title="'+title+ '">' +
        	message +
        '</div>');
		
//		$("#popup_ok").click( function() {
//			$('#modalConfirm').modal('hide');
//			$("#modalConfirm").remove();
//			//callback(true);
//			function_ok();
//		});
		
		$("#modalConfirm").dialog({
            autoOpen: false,
            buttons: {
                Ok: function() {
                	
                	function_ok();
                	$( this ).dialog( "close" );
                }
              }
        });
		
		$("#modalConfirm").dialog('open');
}
function rConfirm(title, message, function_ok) {
		if ($("#modalConfirm").length > 0){
			  // do something here
			$("#modalConfirm").remove();
		}
		
		//alert($("#modalConfirm").length);
		$("BODY").append(
        '<div id="modalConfirm" class="modal hide">' +
            '<div class="modal-header">' +
                '<h4 id="popup_title"></h4>' +
            '</div>' +
            '<div class="modal-body">' +
                '<p id="popup_message"></p>' +
            '</div>' +
            '<div class="modal-footer">' +
                '<button class="btn" data-dismiss="modal" aria-hidden="true">Não</button>' +
                '<button class="btn btn-primary" id="popup_ok">Sim</button>' +
            '</div>' +
        '</div>');
		$('#modalConfirm').modal('show');
		$("#popup_title").text(title);
		$("#popup_message").text(message);
		
		$("#popup_ok").click( function() {
			$('#modalConfirm').modal('hide');
			$("#modalConfirm").remove();
			//callback(true);
			function_ok();
		});
}

function rAlert(title, message, function_ok) {
	if ($("#modalAlert").length > 0){
		  // do something here
		$("#modalAlert").remove();
	}
	
	//alert($("#modalConfirm").length);
	$("BODY").append(
    '<div id="modalAlert" class="modal hide">' +
        '<div class="modal-header">' +
            '<h4 id="popup_title"></h4>' +
        '</div>' +
        '<div class="modal-body">' +
            '<p id="popup_message"></p>' +
        '</div>' +
        '<div class="modal-footer">' +
            '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="popup_ok">Ok</button>' +
        '</div>' +
    '</div>');
	$('#modalAlert').modal('show');
	$("#popup_title").text(title);
	$("#popup_message").text(message);
	
	$("#popup_ok").click( function() {
		$('#modalAlert').modal('hide');
		$("#modalAlert").remove();
		//callback(true);
		if (function_ok != undefined) {
			function_ok();	
		}
		
	});
}

function rProgressBar(title, message, function_ok) {
	if ($("#modalProgressBar").length > 0){
		  // do something here
		$("#modalProgressBar").remove();
	}
	
	//alert($("#modalConfirm").length);
	$("BODY").append(
			'<!-- START Progress Bar -->' +
            '<div  id="modalProgressBar" class="modal hide">' +
            	'<div class="modal-header">' +
            		'<h4 id="popup_title"></h4>' +
            	'</div>' +
                '<section class="body">' +
                    '<div class="modal-body">' +
                    	'<p id="popup_message"></p>' +
                        '<!-- Progress Bar Striped Active -->' +
                        '<div class="progress progress-striped active">' +
                            '<div class="bar" style="width: 100%;"></div>' +
                        '</div><!--/ Progress Bar Striped Active -->' +
                        
                    '</div>' +
                '</section>' +
            '</div>' +
            '<!--/ END Progress Bar -->');
			
//    '<div id="modalAlert" class="modal hide">' +
//        '<div class="modal-header">' +
//            '<h4 id="popup_title"></h4>' +
//        '</div>' +
//        '<div class="modal-body">' +
//            '<p id="popup_message"></p>' +
//        '</div>' +
//        '<div class="modal-footer">' +
//            '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="popup_ok">Ok</button>' +
//        '</div>' +
//    '</div>'
        
	
	$('#modalProgressBar').modal('show');
	$("#popup_title").text(title);
	$("#popup_message").text(message);
	
//	$("#popup_ok").click( function() {
//		$('#modalProgressBar').modal('hide');
//		$("#modalProgressBar").remove();
//		//callback(true);
//		if (function_ok != undefined) {
//			function_ok();	
//		}
//		
//	});
}

